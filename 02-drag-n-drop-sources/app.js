function dragAndDrop() {
  const $placeholders = document.querySelectorAll('.placeholder')
  const $masks = document.querySelectorAll('.mask')
  const $newBlock = document.querySelector('.new-block')
  const $body = document.querySelector('body')
  let activeItem = null

  const addItem = () => {
    const container = $placeholders[0]
    const item = document.createElement('div')
    item.classList.add('item')
    item.setAttribute('draggable', true)
    item.innerText = 'Перетащи меня'
    container.insertAdjacentElement('beforeEnd', item)

    item.addEventListener('dragstart', dragstart)
    item.addEventListener('dragend', dragend)
  }

  const dragstart = (e) => {
    const item = e.target
    item.classList.add('drag')
    $body.classList.add('drag-event')
    activeItem = item
    setTimeout(() => item.classList.add('hidden'), 0)
  }

  const dragend = (e) => {
    const item = e.target
    item.classList.remove('drag', 'hidden')
    $body.classList.remove('drag-event')
    activeItem = null
  }

  const dragover = (e) => {
    e.preventDefault()
  }

  const dragenter = (e) => {
    const elem = e.target
    elem.classList.add('hover')
  }

  const dragleave = (e) => {
    const elem = e.target
    elem.classList.remove('hover')
  }

  const drop = (e) => {
    const elem = e.target
    elem.parentElement.append(activeItem)
    elem.classList.remove('hover')
  }

  const addEvents = () => {
    $newBlock.addEventListener('click', addItem)
    for (const pl of $masks) {
      pl.addEventListener('dragover', dragover)
      pl.addEventListener('dragenter', dragenter)
      pl.addEventListener('dragleave', dragleave)
      pl.addEventListener('drop', drop)
    }
  }

  return {
    init() {
      addItem()
      addEvents()
    },
  }
}

dragAndDrop().init()
