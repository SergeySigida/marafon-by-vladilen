function slider (options) {
    const { slides, activeSlide, arrowsSelector } = options
    const $sidebar = document.querySelector('.sidebar')
    const $main = document.querySelector('.main-slide')
    const slidesCount = slides.length
    const controlUp = document.querySelector(arrowsSelector.up)
    const controlDown = document.querySelector(arrowsSelector.down)
    let activeSlideIndex = activeSlide || 0

    const init = () => {
        slides.forEach(item => {
            $sidebar.style.top = `-${(slidesCount-1) * 100}vh`
            
            $sidebar.insertAdjacentHTML('afterbegin', `
            <div style="background: ${item.background};">
                <h1>${item.title}</h1>
                <p>${item.desc}</p>
            </div>
            `)

            $main.insertAdjacentHTML('beforeend', `
            <div style="
              background-image: url('${item.image}');
            "
            ></div>
            `)
        })

        controlUp.addEventListener('click', up)
        controlDown.addEventListener('click', down)

        document.addEventListener('keyup', keyUp)

        slide()
    }

    const up = () => {
        changeSlide('up')
    }

    const down = () => {
        changeSlide('down')
    }

    const keyUp = e => {
        const key = e.key
        if (key === 'ArrowUp') {
            changeSlide('up')
        } else if (key === 'ArrowDown') {
            changeSlide('down')
        }
    }

    const changeSlide = direction => {
        if (direction === 'up') {
            activeSlideIndex++
            if (activeSlideIndex === slidesCount) activeSlideIndex = 0 

        } else if (direction === 'down') {
            activeSlideIndex--
            if (activeSlideIndex < 0) activeSlideIndex = slidesCount - 1
        }   
        slide()     
    }

    const slide = () => {
        $sidebar.style.transform = `translateY(${activeSlideIndex * 100}vh)`
        $main.style.transform = `translateY(-${activeSlideIndex * 100}vh)`
    }

    return {
        init
    }
}
slider({
    slides: [
        {
            title: 'title1',
            desc: 'desc',
            background: 'linear-gradient(220.16deg, #1f1f1f -8%, #443e3e 138%)',
            image: 'https://images.unsplash.com/photo-1503376780353-7e6692767b70?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MXwxfDB8MXxhbGx8fHx8fHx8fA&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit&auto=format&fit=crop&w=1996&q=80'
        },
        {
            title: 'title2',
            desc: 'desc',
            background: 'linear-gradient(221.87deg, #000000 1%, #182949 128%)',
            image: 'https://images.unsplash.com/photo-1492144534655-ae79c964c9d7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxhbGx8fHx8fHx8fHwxNjIyMjUxOTk0&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit&auto=format&fit=crop&w=2023&q=80'
        },
        {
            title: 'title3',
            desc: 'desc',
            background: 'linear-gradient(221.87deg, #0c3d10 1%, #64b31a 128%)',
            image: 'https://images.unsplash.com/photo-1589536672709-a5d34b12466d?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTYzMTk4NzIwNA&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080&auto=format&fit=crop&w=1950&q=80'
        },
        {
            title: 'title4',
            desc: 'desc',
            background: 'linear-gradient(221.87deg, #da9006 1%, #583e0d 128%)',
            image: 'https://images.unsplash.com/photo-1549288830-f95a7354fea8?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTYzMTk4NzM3NQ&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080&auto=format&fit=crop&w=1950&q=80'
        }
    ],
    // activeSlide: 3,
    arrowsSelector: {
        up: '.up-button',
        down: '.down-button'
    }
}).init()