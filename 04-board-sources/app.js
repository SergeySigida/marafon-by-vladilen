function animationSquare(options) {
    const { container, count } = options
    const $container = document.querySelector(container)
    const init = () => {
        for(let i = 0; i < count; i++) {
            const square = document.createElement('div')
            square.classList.add('square')
            $container.insertAdjacentElement('beforeend', square)

            square.addEventListener('mouseover', mouseover)
            square.addEventListener('mouseleave', mouseleave)
        } 
    }

    const mouseover = e => {
        const elem = e.target
        elem.style.backgroundColor = getColor()
    }

    const mouseleave = e => {
        const elem = e.target
        elem.removeAttribute('style')
    }

    const getColor = () => {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    } 

    return {
        init
    }
}

animationSquare({
    container: '#board',
    count: 800
}).init()