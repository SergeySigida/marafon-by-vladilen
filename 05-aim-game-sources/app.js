class Base {
    $btnStart = document.querySelector('.start')
    $screens = document.querySelectorAll('.screen')
    $timeValue = document.querySelector('.time-value')
    $btnTime = document.querySelectorAll('.time-btn')
    $btnInGame = document.querySelector('.in-game')
    $timeStr = document.querySelector('#time')
    $btnRestart = document.querySelector('#restart')
    $board = document.querySelector('#board')

    time = 5
    score = 0
    step = 0
    interval = null

    constructor () {
        this.init()
        this.events()
    }

    init () {
        this.changeScreen()
        this.drawTimeValue()
    } 

    events () {
        this.$btnStart.addEventListener('click', this.startGame)
        this.$btnTime.forEach(b => b.addEventListener('click', this.changeTime))
        this.$btnInGame.addEventListener('click', this.inGame)
        this.$btnRestart.addEventListener('click', this.restart)
    }

    startGame = e => {
        e.preventDefault()
        this.step++
        this.changeScreen()
        this.drawTimeValue()
    }

    changeTime = e => {
        const time = e.target.getAttribute('data-val')
        this.time += +time
        if (this.time < 5) this.time = 5 
        else if (this.time > 60) this.time = 60
        this.drawTimeValue()
    }

    inGame = e => {
        this.step++
        this.changeScreen()
        this.drawTimeStr()
        this.timer()
        this.renderCircle()
    }

    restart = e => {
        e.preventDefault()
        this.reset()
    }

    timer () {
        this.interval = setInterval(() => {
            this.time--
            this.drawTimeStr()
            if (this.time < 1) {
                clearInterval(this.interval)
                this.finish()
            }            
        }, 1000)
    }

    renderCircle () {
        const size = this.getRandomNum(5, 70)
        const { width, height } = this.$board.getBoundingClientRect()
        const x = this.getRandomNum(size, width - size)
        const y = this.getRandomNum(size, height - size)
        const elem = document.createElement('span')
        elem.classList.add('circle')
        elem.style.top = `${y}px`
        elem.style.left = `${x}px`
        elem.style.width = `${size}px`
        elem.style.height = `${size}px`
        elem.style.background = this.getColor()

        elem.addEventListener('click', () => {
            this.score++
            elem.remove()
            this.renderCircle()
        })

        this.$board.append(elem)
    }

    finish () {
        this.$timeStr.parentElement.classList.add('hide')
        this.$btnRestart.parentElement.classList.remove('hide')
        this.$board.innerHTML = `<h2>Счет: ${this.score}</h2>`
    }

    changeScreen () {
        this.$screens.forEach(i => i.classList.remove('up'))
        for(let i = 0; i < this.step; i++) {
            this.$screens[i].classList.add('up')
        }
    }

    drawTimeValue () {
        this.$timeValue.innerText = this.time
    }

    drawTimeStr () {
        let time = this.time
        if (time < 10) time = `0${time}`
        this.$timeStr.innerText = `00:${time}`
    }

    reset () {
        this.time = 5
        this.score = 0
        this.step = 0
        this.interval = null
        this.changeScreen()

        this.$timeStr.parentElement.classList.remove('hide')
        this.$btnRestart.parentElement.classList.add('hide')
        this.$board.innerText = ''
    }

    getRandomNum (min, max) {
        return Math.round(Math.random() * (max - min) + min)
    }

    getColor = () => {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    } 
}


class Game extends Base {
    
}

new Base()